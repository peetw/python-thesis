import os
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import itertools

# Data filenames
full_path = os.path.realpath(__file__)
base_dir = os.path.dirname((os.path.dirname(full_path)))
FILE_FORCE_DATA = os.path.join(base_dir, 'input', 'F-vs-U.csv')
FILE_PHYS_DATA = os.path.join(base_dir, 'input', 'specimen-properties.csv')
FILE_AP_DATA = os.path.join(base_dir, 'input', 'specimen-Ap.csv')
FILE_AL_DATA = os.path.join(base_dir, 'input', 'specimen-Al.csv')
FILE_AS_DATA = os.path.join(base_dir, 'input', 'specimen-As.csv')
FILE_W_DATA = os.path.join(base_dir, 'input', 'specimen-W.csv')
FILE_AP_H_DATA = os.path.join(base_dir, 'input', 'Ap-vs-H.csv')
FILE_W_H_DATA = os.path.join(base_dir, 'input', 'W-vs-H.csv')

# Read in data and store in pandas DataFrames
phys_prop = pd.read_csv(FILE_PHYS_DATA, skiprows=(1,7,16), header=0, index_col=['Species','Specimen'])
projected_area = pd.read_csv(FILE_AP_DATA, skiprows=(1,7,16), header=0, index_col=['Species','Specimen'])
leaf_area = pd.read_csv(FILE_AL_DATA, skiprows=(1,7,16), header=0, index_col=['Species','Specimen'])
stem_area = pd.read_csv(FILE_AS_DATA, skiprows=(1,6,11), header=0, index_col=['Species','Specimen'])
stem_area = pd.DataFrame(stem_area['As'].groupby(level=[0,1]).sum()).rename(columns={0: 'As'})
width = pd.read_csv(FILE_W_DATA, skiprows=(1,7,16), header=0, index_col=['Species','Specimen'])

data = phys_prop.join([projected_area,leaf_area,stem_area,width])

drag_force = pd.read_csv(FILE_FORCE_DATA, header=[0,1,2], tupleize_cols=False)

projected_area_height = pd.read_csv(FILE_AP_H_DATA, header=[0,1,2], tupleize_cols=False)
width_height = pd.read_csv(FILE_W_H_DATA, header=[0,1,2], tupleize_cols=False)

# Print data
#pd.set_option('display.max_columns', None)
#print data, "\n\nWillow mean Ap0 = ", data.ix['Willow']['Ap_fol'].mean()
#print "\n", drag_force, "\n\nAlder velocities\n", drag_force['Alder'].xs('U', axis=1, level=1)
#print "\n", projected_area_height, "\n\nAlder North\n", projected_area_height['Alder'].xs('North', axis=1, level=1)
#print "\n", width_height, "\n\nAlder North\n", width_height['Alder'].xs('North', axis=1, level=1)

#
fig = plt.figure(1)
ax = fig.add_subplot(2,1,1)
lines = ax.plot(drag_force['Alder'].xs('U', axis=1, level=1), drag_force['Alder'].xs('F_fol', axis=1, level=1))
plt.xlabel(r'$U$ (m s$^{-1}$)')
plt.ylabel(r'$F$ (N)')
plt.axis([0, 3, 0, 200])
plt.legend(drag_force['Alder'].xs('U', axis=1, level=1).columns.values, loc='upper center', bbox_to_anchor=(0.5,1.25), ncol=5, fancybox=True, shadow=True)

plt.subplot(2,1,2)
plt.plot(drag_force['Alder'].xs('U', axis=1, level=1), drag_force['Alder'].xs('F_defol', axis=1, level=1))
plt.xlabel(r'$U$ (m s$^{-1}$)')
plt.ylabel(r'$F$ (N)')
plt.axis([0, 3, 0, 200])

for l, ms in zip(ax.lines, itertools.cycle('>^+*')):
    l.set_marker(ms)
    l.set_color('black')

#plt.tight_layout()
plt.show()
